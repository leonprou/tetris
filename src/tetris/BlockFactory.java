/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris;

import com.jme3.app.Application;
import com.jme3.bounding.BoundingBox;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;

/**
 *
 * @author leonprou
 */
public class BlockFactory {
    

    public static Geometry produceBlock(ColorRGBA color) {
        
        Geometry geometry = new Geometry("Block", new Box(0.5f, 0.5f, 0.5f));
        geometry.setModelBound(new BoundingBox());
        geometry.updateModelBound();
        geometry.setUserData("draggable", true);
        Material mat1 = new Material(Tetris.getApp().getAssetManager(),
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat1.setColor("Color", color);
        geometry.setMaterial(mat1);
        return geometry;
    }
}
