/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris;

import tetris.board.Board;
import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;

/**
 *
 * @author root
 */
public class FallingBlock extends AbstractAppState {

    private Tetris app;
    private Board board;
    private Geometry fallingGeometry;
    private Geometry suggestedGeometry;
    private Vector3f location;
    private float tpfSum;
    private boolean isLanded;

    public FallingBlock(Vector3f location) {
        this.location = location;
    }

    @Override
    public void initialize(AppStateManager stateManager,
            Application app) {
        this.app = (Tetris) app;
        this.board = this.app.getWorld().getBoard();

        setFallingGeometry(BlockFactory.produceBlock(ColorRGBA.Red));
        getFallingGeometry().setLocalTranslation(location);
        this.app.getRootNode().attachChild(getFallingGeometry());
        suggestedGeometry = getFallingGeometry().clone();
        suggestedGeometry.getMaterial().setColor("Color", ColorRGBA.Green);
        Vector3f landingLocation = board.getTopLocation(getFallingGeometry().getLocalTranslation());
        suggestedGeometry.setLocalTranslation(landingLocation);
        this.app.getRootNode().attachChild(suggestedGeometry);
    }

    @Override
    public void update(float tpf) {
        tpfSum += tpf;
        if (tpfSum > World.getGameSpeed()) {

            if (isLanded == false) {
                getFallingGeometry().move(0, -1, 0);
            } else {
                respawn();
            }

            if (board.landed(getFallingGeometry())) {
                app.getRootNode().detachChild(suggestedGeometry);
                isLanded = true;
            }
            tpfSum = 0;
        }
    }

    private void respawn() {
        getFallingGeometry().setUserData("draggable", false);
        app.getStateManager().detach(this);
        board.addToBoard(fallingGeometry);
        app.getWorld().respawnBlock();
    }

    public void land() {
        app.getRootNode().detachChild(fallingGeometry);
        suggestedGeometry.getMaterial().setColor("Color", ColorRGBA.Red);
        setFallingGeometry(suggestedGeometry);
        respawn();
    }

    /**
     * @return the fallingGeometry
     */
    public Geometry getFallingGeometry() {
        return fallingGeometry;
    }

    /**
     * @param fallingGeometry the fallingGeometry to set
     */
    public void setFallingGeometry(Geometry fallingGeometry) {
        this.fallingGeometry = fallingGeometry;
    }

    /**
     * @return the suggestedGeometry
     */
    public Geometry getSuggestedGeometry() {
        return suggestedGeometry;
    }

    /**
     * @param suggestedGeometry the suggestedGeometry to set
     */
    public void setSuggestedGeometry(Geometry suggestedGeometry) {
        this.suggestedGeometry = suggestedGeometry;
    }

}