package tetris;

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.system.AppSettings;

/**
 * test
 *
 * @author normenhansen
 */
public class Tetris extends SimpleApplication {

    private World world;
    private static Tetris app;
    
    public static void main(String[] args) {

        Tetris.app = new Tetris();

        AppSettings settings = new AppSettings(true);
        settings.setTitle("Tetris 3D");
        Tetris.app.setSettings(settings);
        Tetris.app.setShowSettings(false);

        Tetris.app.start();
    }

    @Override
    public void simpleInitApp() {
        world = new World(this);
//        initCrossHairs();

    }

    protected void initCrossHairs() {
        setDisplayStatView(false);
        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        BitmapText ch = new BitmapText(guiFont, false);
        ch.setSize(guiFont.getCharSet().getRenderedSize() * 2);
        ch.setText("+"); // crosshairs
        ch.setLocalTranslation( // center
                settings.getWidth() / 2 - ch.getLineWidth() / 2, settings.getHeight() / 2 + ch.getLineHeight() / 2, 0);
        guiNode.attachChild(ch);
    }

    /**
     * @return the worldState
     */
    public World getWorld() {
        return world;
    }

    /**
     * @return the app
     */
    public static Tetris getApp() {
        return app;
    }
}
