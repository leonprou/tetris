/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import java.util.Random;
import tetris.board.Board;
import tetris.player.Player;

/**
 *
 * @author leonprou
 */
public class World {

    private Tetris app;
    private Board board;
    private Player player;
    private Random random = new Random();
    private FallingBlock fallingBlock;
    
    public static float stepTime = 0.5f;
    private final static int worldSizeX = 10;
    private final static int worldSizeZ = 10;
    private static float gameSpeed = 0.5f;
   

    public World(Tetris app) {
        this.app = (Tetris) app;
        board = new Board(this.app);
//        app.getStateManager().attach(new WorldState());
        this.player = new Player();
        app.getStateManager().attach(this.player);
        respawnBlock();
    }

//    public static boolean legitLocation(Vector3f location) {
//        if ((location.x < worldSizeX / 2 || location.x > worldSizeX / -2)
//                && (location.y < worldSizeY / 2 || location.y > worldSizeY / -2)
//                && (location.z < worldSizeZ / 2 || location.z > worldSizeZ / -2)) {
//            return true;
//        }
//        return false;
//    }
    public static Vector3f pointToLocation(Vector2f point) {
        return Tetris.getApp().getCamera()
                .getWorldCoordinates(new Vector2f(point.x, point.y), 0f).clone();
    }

    public static Vector3f locationToTile(Vector3f location) {
        Vector3f tile = new Vector3f();
        tile.x = Math.round(location.x) + 0.5f;
        tile.y = Math.round(location.y)+ 0.5f;
        tile.z = Math.round(location.z)+ 0.5f;

        return tile;
    }
    
    public final void respawnBlock() {
        fallingBlock = new FallingBlock(randomWorldTopLocation());
        app.getStateManager().attach(getFallingBlock());
    }
    
//    public void landBlock(Geometry block) {
//        if (player.getSuggestedGeometry() != null) {
//            app.getRootNode().detachChild(block);
//            player.getSuggestedGeometry().getMaterial().setColor("Color", ColorRGBA.Red);
//            getBoard().addToBoard(player.getSuggestedGeometry());
//        }
//    }
    
    private Vector3f randomWorldTopLocation() {

        return new Vector3f(random.nextInt(getWorldSizeX()) - 4.5f, 10.5f, random.nextInt(getWorldSizeZ()) - 4.5f);
    }

    /**
     * @return the board
     */
    public Board getBoard() {
        return board;
    }

    /**
     * @return the worldSizeX
     */
    public static int getWorldSizeX() {
        return worldSizeX;
    }

    /**
     * @return the worldSizeY
     */
//    public static int getWorldSizeY() {
//        return worldSizeY;
//    }

    /**
     * @return the worldSizeZ
     */
    public static int getWorldSizeZ() {
        return worldSizeZ;
    }

    /**
     * @return the gameSpeed
     */
    public static float getGameSpeed() {
        return gameSpeed;
    }

    /**
     * @return the playerState
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * @param playerState the playerState to set
     */
    public void setPlayerState(Player playerState) {
        app.getStateManager().detach(this.player);
        app.getStateManager().attach(playerState);
        this.player = playerState;
    }

    /**
     * @return the fallingBlock
     */
    public FallingBlock getFallingBlock() {
        return fallingBlock;
    }
}
