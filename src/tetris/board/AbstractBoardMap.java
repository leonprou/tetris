/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris.board;

import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author leonprou
 */
public abstract class AbstractBoardMap {
    
    public abstract boolean isFree(Vector3f location);
    
    public abstract void addGeometry(Geometry block);
    
    public abstract void deleteGeometry(Geometry geometry);
    
    public abstract ArrayList<Geometry> getGeometriesRow(Vector3f location, Axis axis);
    
    public abstract ArrayList<Geometry> getGeometriesSequence(Vector3f location, Axis axis);
    
    public abstract void moveTo(Geometry geometry, Vector3f delta);

}
