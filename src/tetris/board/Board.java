/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris.board;

import com.jme3.collision.CollisionResults;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;
import java.util.ArrayList;
import tetris.Tetris;
import tetris.World;

/**
 *
 * @author leonprou
 */
public class Board {

    private Tetris app;
    private AbstractBoardMap boardMap;
    private Node boardNode;
    private Geometry board;

    public Board(Tetris app) {
        this.app = app;
        boardNode = new Node("Board node");
        app.getRootNode().attachChild(boardNode);
        boardMap = new HashBoardMap(app);
        initFloor();
    }

    public boolean isFree(Vector3f location) {
        return boardMap.isFree(location);
    }

    public boolean landed(Geometry geometry) {
        return geometry.getLocalTranslation().y <= 0.5
                || boardMap.isFree(geometry.getLocalTranslation().add(new Vector3f(0, -1, 0)));
    }

    public void addToBoard(Geometry geometry) {
        boardMap.addGeometry(geometry);
        getBoardNode().attachChild(geometry);
        // Checking if it's a full line
        Vector3f location = geometry.getLocalTranslation();
        ArrayList<Geometry> xRow = boardMap.getGeometriesRow(location, Axis.X);
        ArrayList<Geometry> zRow = boardMap.getGeometriesRow(location, Axis.Z);

        if (xRow.size() != 10 && zRow.size() != 10) {
            return;
        }

        ArrayList<Geometry> deletedGeometries = deleteGeometries(xRow, zRow);
        collapseColumns(deletedGeometries);
    }

    public Vector3f getTopLocation(Vector3f location) {
        CollisionResults results = new CollisionResults();
        Ray ray = new Ray(location, new Vector3f(0, -1, 0));
        boardNode.collideWith(ray, results);
        Geometry topGeometry = results.getClosestCollision().getGeometry();
        if (topGeometry.getName().equals("Board")) {
            return location.clone().setY(0.5f);
        } else {
            return topGeometry.getLocalTranslation();
        }
    }

    private void initFloor() {
        Box box = new Box(World.getWorldSizeX() / 2, .2f, World.getWorldSizeZ() / 2);
        board = new Geometry("Board", box);
        board.setLocalTranslation(0, -0.2f, 0);
        Material mat1 = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
        mat1.setColor("Color", ColorRGBA.Gray);
        board.setMaterial(mat1);

        getBoardNode().attachChild(board);
    }

    private void collapseColumns(ArrayList<Geometry> toCollapse) {
        for (Geometry geometry : toCollapse) {
            ArrayList<Geometry> upperGeometries =
                    boardMap.getGeometriesSequence(geometry.getLocalTranslation(),
                    Axis.Y);
            for (Geometry upperGeometry : upperGeometries) {
                collapseGeometry(upperGeometry);
            }
        }
    }

    private void collapseGeometry(Geometry geometry) {
        Vector3f location = geometry.getLocalTranslation().add(new Vector3f(0, -1, 0));
        boardMap.moveTo(geometry, location);
        geometry.setLocalTranslation(location);
    }

    private ArrayList<Geometry> deleteGeometries(ArrayList<Geometry> xRow, ArrayList<Geometry> zRow) {
        ArrayList<Geometry> geometiesToDelete;
        if (xRow.size() == 10 && zRow.size() == 10) {
            geometiesToDelete = new ArrayList<Geometry>();
            geometiesToDelete.addAll(xRow);
            geometiesToDelete.addAll(zRow);
        } else if (xRow.size() == 10) {
            geometiesToDelete = xRow;
        } else {
            geometiesToDelete = zRow;
        }
        for (Geometry geometry : geometiesToDelete) {
            boardMap.deleteGeometry(geometry);
            boardNode.detachChild(geometry);
        }
        return geometiesToDelete;
    }

    public boolean checkRows(Vector3f location) {
        ArrayList<Geometry> xRow = boardMap.getGeometriesRow(location, Axis.X);
        if (xRow.size() == 10) {
            return true;
        }

        ArrayList<Geometry> zRow = boardMap.getGeometriesRow(location, Axis.Z);
        if (zRow.size() == 10) {
            return true;
        }
        return false;
    }

    //    private void initLines() {
//        for (int i = 0; i < 11; i++) {
//            Line line = new Line(
//                    new Vector3f(i - 5, -3.79f, 0), new Vector3f(i - 5, -3.79f, -10));
//            Geometry seperator = new Geometry("Line", line);
//            Material mat1 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
//            mat1.setColor("Color", ColorRGBA.Blue);
//            seperator.setMaterial(mat1);
//            rootNode.attachChild(seperator);
//        }
//
//        for (int i = 0; i < 11; i++) {
//            Line line = new Line(
//                    new Vector3f(-5, -3.79f, i-10), new Vector3f(5, -3.79f, i-10));
//            Geometry seperator = new Geometry("Line", line);
//            Material mat1 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
//            mat1.setColor("Color", ColorRGBA.Blue);
//            seperator.setMaterial(mat1);
//            rootNode.attachChild(seperator);
//        }
//
//    }
    /**
     * @return the boardNode
     */
    public Node getBoardNode() {
        return boardNode;
    }

    /**
     * @param boardNode the boardNode to set
     */
    public void setBoardNode(Node boardNode) {
        this.boardNode = boardNode;
    }
}
