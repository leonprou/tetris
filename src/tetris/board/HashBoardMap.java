/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris.board;

import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import tetris.Tetris;

/**
 *
 * @author root
 */
public class HashBoardMap extends AbstractBoardMap {

    private HashMap<Vector3f, Geometry> blocks =
            new HashMap<Vector3f, Geometry>();
    private Vector3f downLeftCorner = new Vector3f(-5.5f, 0, 5.5f);
    private final Tetris app;

    public class BoardMapIterator implements Iterator<Geometry> {

        private Vector3f step;
        private Vector3f cur;

        public BoardMapIterator(Vector3f start, Vector3f step) {
            this.cur = start;
            this.step = step;
        }

        public boolean hasNext() {
//            return World.legitLocation(cur.add(step));
            return blocks.containsKey(cur.add(step));
        }

        public Geometry next() {
            cur = cur.add(step);
            return blocks.get(cur);
        }

        public void remove() {
            blocks.remove(cur);
            cur = cur.add(step);
        }
    }
    
    public HashBoardMap(Tetris app) {
        this.app = app;
    }

    @Override
    public ArrayList<Geometry> getGeometriesRow(Vector3f location, Axis axis) {
        Iterator<Geometry> iterator = iterator(location, axis);

        return iterateBy(iterator);
    }
    
    @Override
    public ArrayList<Geometry> getGeometriesSequence(Vector3f location, Axis axis) {
        Iterator<Geometry> iterator = new BoardMapIterator(location, calcStep(axis));
        return iterateBy(iterator);
    }
    

    private ArrayList<Geometry> iterateBy(Iterator<Geometry> iterator) {
        ArrayList<Geometry> geometriesRow = new ArrayList<Geometry>();
        while (iterator.hasNext()) {
            Geometry geometry = iterator.next();
            geometriesRow.add(geometry);
        }
        return geometriesRow;
    }

    @Override
    public boolean isFree(Vector3f location) {
        return blocks.containsKey(location);
    }

    @Override
    public void addGeometry(Geometry block) {
        blocks.put(block.getLocalTranslation(), block);
    }

    @Override
    public void deleteGeometry(Geometry geometry) {
        blocks.remove(geometry.getLocalTranslation());
    }
    
    @Override
    public void moveTo(Geometry geometry, Vector3f location) {
        blocks.remove(geometry.getLocalTranslation());
        blocks.put(location, geometry);
    }

    public Iterator<Geometry> iterator(Vector3f location, Axis axis) {
        Vector3f start, step;
        if (axis == Axis.X) {
            start = new Vector3f(downLeftCorner.x, location.y, location.z);
        } else if (axis == Axis.Z) {
            start = new Vector3f(location.x, location.y, downLeftCorner.z);
        } else {
            return null;
        }
        step = calcStep(axis);
        return new BoardMapIterator(start, step);
    }

    private Vector3f calcStep(Axis axis) {
        switch (axis) {
            case X:
                return new Vector3f(1, 0, 0);
            case Y:
                return new Vector3f(0, 1, 0);
            case Z:
                return new Vector3f(0, 0, -1);
            default:
                return null;
        }
    }
}
