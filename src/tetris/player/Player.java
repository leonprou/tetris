/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris.player;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.input.ChaseCamera;
import static com.jme3.input.ChaseCamera.ChaseCamDown;
import static com.jme3.input.ChaseCamera.ChaseCamMoveLeft;
import static com.jme3.input.ChaseCamera.ChaseCamMoveRight;
import static com.jme3.input.ChaseCamera.ChaseCamUp;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.math.FastMath;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import tetris.FallingBlock;
import tetris.Tetris;
import tetris.World;
import tetris.board.Board;

/**
 *
 * @author root
 */
public class Player extends AbstractAppState {

    private Tetris app;
    private ChaseCamera chaseCamera;
    private FallingBlock fallingBlock;
    private Board board;
    private boolean isDragging;
    private ActionListener initDragListener = new ActionListener() {
        public void onAction(String name, boolean isPressed, float tpf) {
            if (name.equals("initDrag")) {
                // Dropping the geometry
                if (isDragging) {
//                    if (moveBlock()) {
                    isDragging = false;
                    chaseCamera.setEnabled(true);
//                    }
                } // Start the drag
                else {
                    CollisionResults results = calcCollisionsWith(app.getRootNode());
                    if (results.size() > 0) {

                        CollisionResult draggable = getDraggableResult(results);
                        if (draggable != null) {
                            isDragging = true;
                            System.out.println("  You choose " + draggable.getGeometry().getName() + " at " + draggable.getContactPoint() + ", " + draggable.getDistance() + " wu away.");
                            chaseCamera.setEnabled(false);
                        }
                    }
                }
            }
        }
    };
    private ActionListener dropListener = new ActionListener() {
        public void onAction(String name, boolean isPressed, float tpf) {
            if (name.equals("drop") && isPressed) {
//                if (fallingBlock == null) {
//                    fallingBlock = app.getWorld().getFallingBlock();
//                }
                System.out.println("drop");
                app.getWorld().getFallingBlock().land();
            }
        }
    };
    private AnalogListener dragListener = new AnalogListener() {
        public void onAnalog(String name, float value, float tpf) {
            if (isDragging) {
                CollisionResult result = calcCollisionsWith(board.getBoardNode())
                        .getClosestCollision();
                if (result != null) {
                    Vector3f location = World.locationToTile(result.getContactPoint());
                    FallingBlock fallingBlock = app.getWorld().getFallingBlock();
                    Geometry suggestedGeometry = fallingBlock.getSuggestedGeometry();
                    Geometry fallingGeometry = fallingBlock.getFallingGeometry();
                    if (!location.equals(suggestedGeometry.getLocalTranslation())) {
                        suggestedGeometry.setLocalTranslation(location);
                        location.y = fallingGeometry.getLocalTranslation().y;
                        fallingGeometry.setLocalTranslation(location);
                    }
                }
            }
        }
    };

    @Override
    public void initialize(AppStateManager stateManager,
            Application app) {

        this.app = (Tetris) app;
        this.app.getFlyByCamera().setEnabled(false);
        chaseCamera = new ChaseCamera(this.app.getCamera(), this.app.getRootNode(), this.app.getInputManager());
        board = this.app.getWorld().getBoard();
        getChaseCamera().setDefaultDistance(15);
        getChaseCamera().setDefaultVerticalRotation((30 / 180f) * FastMath.PI);
        initKeys(app.getInputManager());
    }

    private void initKeys(InputManager inputManager) {
        inputManager.addMapping("initDrag",
                new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        inputManager.addListener(initDragListener, "initDrag");

        inputManager.addMapping("drop", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addListener(dropListener, "drop");

        inputManager.addListener(dragListener, ChaseCamDown, ChaseCamUp,
                ChaseCamMoveLeft, ChaseCamMoveRight);

    }

    private CollisionResults calcCollisionsWith(Spatial spatial) {
        CollisionResults results = new CollisionResults();

        Vector2f click2d = app.getInputManager().getCursorPosition();
        Vector3f click3d = World.pointToLocation(click2d);
        Vector3f dir = app.getCamera().getWorldCoordinates(new Vector2f(click2d.x, click2d.y), 1f)
                .subtractLocal(click3d).normalizeLocal();
        Ray ray = new Ray(click3d,
                dir);
        spatial.collideWith(ray, results);
        return results;
    }

    private CollisionResult getDraggableResult(CollisionResults results) {
        for (CollisionResult result : results) {
            if (result.getGeometry().getUserData("draggable") == Boolean.TRUE) {
                return result;
            }
        }
        return null;
    }

    /**
     * @return the chaseCamera
     */
    public ChaseCamera getChaseCamera() {
        return chaseCamera;
    }
}
